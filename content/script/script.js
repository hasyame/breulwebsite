var csrf_hash;

$('body').ready(function(){
	csrf_hash = $('input[name="csrf_token"]').attr('value');
	ajax_form();
});

function update_csrf(new_csrf) {
	csrf_hash = new_csrf;
	$('input[name="csrf"]').attr('value', new_csrf);
}

function ajax_form() {
	$('form')
		.submit(function(e) {
			e.preventDefault();
			$.post(
				base_url+'contact/submit',
				{
					csrf_token	: csrf_hash,
					objet		: $('#objet').val(),
					email 		: $('#email').val(),
					message		: $('#message').val(),
				},
				function(data, statuts) {
					update_csrf(data.csrf);
					$('.validation').remove();
					if(data.success) {
						$('section.content').prepend('<div id="success" class="validation"><div class="symbole">v</div><div><p>Message envoyÃ© avec succÃ¨s<p></div></div><br>');
					} else {
						$('section.content').prepend('<div id="errors" class="validation"><div class="symbole">x</div><div>'+data.reason+'</div></div><br>');
					}
				}
			);
		});
}
