<html>
  <head>
    <title>Benoît Breul - Expert Cybersécurité</title>
  	<meta charset="UTF-8" />
  	<link rel="stylesheet" type="text/css" href="./content/css/design.css">
  	<link rel="stylesheet" type="text/css" href="./content/css/design_flex.css">	<meta name=viewport content="width=device-width, initial-scale=1">
  	<meta name="author" content="Benoît Breul">
  	<meta name="keywords" content="Expert cybersécurité, sécurité informatique, analyste, pilotage, projets">
  	<meta name="description" content="Expertise en sécurité informatique pour les entreprises de toutes tailles.">
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  	<script type="text/javascript" src="/assets/script.js"></script>
  	<script>
  	var base_url = "/index.php//";
  	function openMenu() {
  		document.getElementById("mainMenu").className = (document.getElementById("mainMenu").className == 'opened')? 'closed':'opened';
  	}
  	</script>
  </head>
  <body>
    <section id="main-display" class="acceuil">
      <header>
        <a href="./index.php">
          <div id="titres">
            <h1>Breul Benoît</h1>
            <h2>Expert en cybersécurité</h2>
          </div>
        </a>
        <nav id="MainMenu" onclick="openMenu()">
          <div id="hamburger">
            <span></span>
          </div>
          <ul>
            <li class="active">
              <a href="/index.php/acceuil">acceuil</a>
            </li>
            <li class="">
                <a href="/index.php/tarifs">tarifs</a>
            </li>
            <li class="">
              <a href="/index.php/tarifs">devis</a>
            </li>
            <li class="">
              <a href="/index.php/tarifs">A propos</a>
            </li>
            <li class="">
              <a href="/index.php/tarifs">contact</a>
            </li>
          </ul>
        </nav>
      </header>

      <section class="content">
        <article id="photoQuestion">
          <div class="photo">
            <img src="./content/photo/acceuil_front.jpeg"
          </div>
          <div class="cadrePhrase">
    				<h3 class="question">Besoin d'expertise en cybersécurité?</h3>
    				<h3 class="reponse">Contactez moi !</h3>
			   </div>
        </article>
      </section>
    </section>

    <section class="acceuil content" id="services">
      <h2>Mes services</h2>
      <ul>
        <li>
          <div>

          </div>
        </li>
      </ul>
    </section>
  </body>
</html>
